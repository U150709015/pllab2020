#include <stdio.h>

int nrDigits(int num)
{
    static int count = 0;

    if(num > 0){
        count++;
        nrDigits(num/10);
    } else {
        return count;
    }
}

int main(void) {

    int number;
    int count = 0;

    printf("Enter a positive number:");
    scanf("%d", &number);

    count = nrDigits(number);


    printf(" %d\n",count);

    return 0;


}
