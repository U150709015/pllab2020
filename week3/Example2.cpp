#include <stdio.h>

int gcd(int m, int n){

    int ans;

    if(m%n==0)
        ans = n;
    else
        ans = gcd(n, m%n);

    return ans;


}

int main(){

    int num1;
    int num2;

    printf("Enter two integers:");
    scanf("%d %d", &num1, &num2);
    printf("%d", gcd(num1,num2));
    return 0;
}

